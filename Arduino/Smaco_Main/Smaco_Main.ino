#include <NTPClient.h> // NTP Module library
#include <WiFiUdp.h> // UDP
#include <ESP8266WiFi.h> //wifi module library
#include <FirebaseArduino.h> //servo motor module library
#include <Servo.h> //servo motor module library
#include <OneWire.h> 
#include <DallasTemperature.h>

// SetWiFi SSID and Password
#define wifiSSID "Your SSID"
#define wifiPassword "Your Password SSID"

// Set Firebase Host and Authentication
#define firebaseHost "Your Firebase Host"
#define firebaseAuth "Your Firebase Authentication"

WiFiUDP ntpUDP;
Servo myservo;  // create servo object to control a servo 
#define servo D9 // pin servo to GIO2

// UDP, time server pool, offset in seconds, and interval in miliseconds
NTPClient timeClient(ntpUDP, "id.pool.ntp.org", 25200, 60000);
#define relay1 D0 // pin relay to GIO12
#define relay2 D1 // pin relay to GIO13

#define tempSensor D8 //pin temperature GIO 14
OneWire oneWire(tempSensor); // Setup a oneWire instance to communicate with any OneWire devices  
DallasTemperature mytemp(&oneWire);// Pass our oneWire reference to Dallas Temperature. 

String pathLamp = "lamp/001";
String pathMotor = "motor/001";
String pathTemp = "temperature/001";


void setupFirebase(){
  //To Connect Firebase
  Firebase.begin(firebaseHost, firebaseAuth);
  Serial.println("Connected Firebase");
}

void setupWiFi(){
  //To Connect the WiFi

  
  WiFi.begin(wifiSSID, wifiPassword);
  Serial.print("WiFi connecting");
  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(500); 
  }
  Serial.println();
  Serial.println("WiFi connected to:");
  Serial.println(WiFi.SSID());
  Serial.println("IP Address:");
  Serial.println(WiFi.localIP());

}

void callLamp(bool led){

  Serial.println();
  Serial.println("Led: ");
  Serial.println(led);

  if (led == true){
    digitalWrite(relay1, LOW);
  }  else if (led == false){
    digitalWrite(relay1, HIGH);
  }

}

void callServo(bool motor) {
  int pos;

  Serial.println();
  Serial.println("Motor: ");
  Serial.println(motor);

  for(pos = 0; pos <= 180; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(10);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=0; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(10);                       // waits 15ms for the servo to reach the position 
  } 

  pos = 0;
  myservo.write(pos);

}

void callTemp(){
  mytemp.requestTemperatures(); // Send the command to get temperature readings 
  Serial.print("Temperature is: "); 
  Serial.print(mytemp.getTempCByIndex(0));
  delay(100); 
  float tempAquarium = mytemp.getTempCByIndex(0);

  if (tempAquarium <= 27.5){
    digitalWrite(relay2, LOW);
  }
  else if (tempAquarium >= 30.5){
    digitalWrite(relay2, HIGH);
  }
  
  String path = pathTemp + "/tempAquarium"; 
  Firebase.setFloat(path, tempAquarium);
}

void setup() {
  Serial.begin(115200);
  
  myservo.write(0); // to make position 0, init pos
  myservo.attach(servo);  // attaches the servo on GIO2 to the servo object 

  mytemp.begin(); 

  timeClient.begin();

  digitalWrite(relay1, HIGH);
  digitalWrite(relay2, HIGH);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  
  setupWiFi();

  setupFirebase();

}

void loop() {
  timeClient.update();

  FirebaseObject motorObject = Firebase.get(pathMotor);
  bool motorCondition = motorObject.getBool("condition");
  int hourServo = motorObject.getInt("hour");
  int minServo = motorObject.getInt("min");
  bool timerServoCondition = motorObject.getBool("timerCondition");

  FirebaseObject lampObject = Firebase.get(pathLamp);
  bool lampCondition = lampObject.getBool("condition");
  int onLampHour = lampObject.getInt("onHour");
  int onLampMin = lampObject.getInt("onMin");
  int offLampHour = lampObject.getInt("offHour");
  int offLampMin = lampObject.getInt("offMin");
  bool timerLampCondition = lampObject.getBool("timerCondition");

  
  
  if (motorCondition == true) {
    callServo(motorCondition);
    motorCondition = false;
    String path = pathMotor + "/condition";
    Firebase.setBool(path, motorCondition); // Update Lamp database to False
  } 
  else if ( timerServoCondition == true && hourServo == timeClient.getHours() && minServo == timeClient.getMinutes() && timeClient.getSeconds() == 0 ){
    motorCondition = true;
    String path = pathMotor + "/condition";
    Firebase.setBool(path, motorCondition); // Update Lamp database to True
    callServo(motorCondition);
    motorCondition = false;
    Firebase.setBool(path, motorCondition); // Update Lamp database to False
  }
  /*else{
    Serial.println(timerServoCondition);
    Serial.print(hourServo);
    Serial.print(minServo);
    Serial.println(timeClient.getFormattedTime());  
  } // for Debugging */

  callLamp(lampCondition); // Turn lamp directly
  if ( timerLampCondition == true && onLampHour == timeClient.getHours() && onLampMin == timeClient.getMinutes() && timeClient.getSeconds() == 0 ){
    callLamp(true);
    lampCondition = true;
    String path = pathLamp + "/condition";
    Firebase.setBool(path, lampCondition); // Update Lamp database to False
  }
  else if ( timerLampCondition == true && offLampHour == timeClient.getHours() && offLampMin == timeClient.getMinutes() && timeClient.getSeconds() == 0 ){
    callLamp(false);
    lampCondition = false;
    String path = pathLamp + "/condition";
    Firebase.setBool(path, lampCondition); // Update Lamp database to False
  }
  /*else{
    Serial.print(onLampHour);
    Serial.println(onLampMin);
    Serial.print(offLampHour);
    Serial.println(offLampMin);
    Serial.println(timeClient.getFormattedTime());  
  } // for Debugging 
  */ 
  
  callTemp();
}
