import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedFishPage } from './feed-fish';

@NgModule({
  declarations: [
    FeedFishPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedFishPage),
  ],
})
export class FeedFishPageModule {}
