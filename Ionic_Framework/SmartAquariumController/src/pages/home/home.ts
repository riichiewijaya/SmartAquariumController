import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { temperatureModel } from '../../models/temperature/temperature.model';
import { TemperatureServices } from '../../services/temperature/temperature.services';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  temperature$: Observable<temperatureModel[]>

  constructor(public navCtrl: NavController, private temperatureS: TemperatureServices) { 
    this.temperature$ = this.temperatureS
    .getTemperature()
    .snapshotChanges()
    .map(
      changes => {
        return changes.map(c =>({
          key:c.payload.key,...c.payload.val()
        }))
      }
    )
  }

  getTemperature(){
    this.temperatureS.getTemperature();
  }
}
