import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TurnLampPage } from './turn-lamp';

@NgModule({
  declarations: [
    TurnLampPage,
  ],
  imports: [
    IonicPageModule.forChild(TurnLampPage),
  ],
})
export class TurnLampPageModule {}
