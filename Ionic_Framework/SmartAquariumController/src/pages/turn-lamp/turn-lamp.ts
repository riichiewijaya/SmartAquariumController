import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { lampModel } from '../../models/lamp/lamp.model';
import { LampServices } from '../../services/lamp/lamp.services';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-turn-lamp',
  templateUrl: 'turn-lamp.html',
})
export class TurnLampPage {
  lamp$: Observable<lampModel[]>;
   // item: lampModel;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private lampS: LampServices
  ) {
    this.lamp$ = this.lampS
    .getLamp()
    .snapshotChanges()
    .map(
      changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }))
      }
    )
  }

  turnLamp(lamp: lampModel, conLamp:boolean){
    this.lampS.turnLamp(lamp, conLamp);
  }

  getTimer(lamp: lampModel, timeOn: Date, timeOff: Date){
    timeOn = new Date(lamp.onTime);
    timeOff = new Date (lamp.offTime);
    let onHour = timeOn.getUTCHours(); // if getHours() it will get GMT+7
    let onMin =  timeOn.getUTCMinutes();
    let offHour = timeOff.getUTCHours(); // if getHours() it will get GMT+7
    let offMin =  timeOff.getUTCMinutes();
    let timer = true;
    console.log(onHour + ":" + onMin + "&" + offHour + offMin);
    console.log(timeOn + ":" + timeOff);
    this.lampS.timerOn(lamp, onHour, onMin, timer, timeOn);
    this.lampS.timeOff(lamp, offHour, offMin, timeOff);
  }

  offTimer(lamp: lampModel, condition: boolean){
    this.lampS.turnOffTimer(lamp, condition);
  }

}
