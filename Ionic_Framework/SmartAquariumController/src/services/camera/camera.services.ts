import { Injectable } from "@angular/core";

import { AngularFireDatabase } from "angularfire2/database";
import { cameraModel } from "../../models/camera/camera.model";


@Injectable()
export class CameraServices{
    private cameraRef = this.db.list<any>('camera');
    
    constructor(private db: AngularFireDatabase){}

    getCamera(){
        return this.cameraRef;
    }

    takeAPict(cam: cameraModel, conCam: boolean, camLastTake: string){
        return this.cameraRef.update(cam.key, {
            condition: conCam,
            lastTake: camLastTake
        });
    }
}