import { Injectable } from "@angular/core";

import { AngularFireDatabase } from "angularfire2/database";
import { motorModel } from "../../models/motor/motor.model";


@Injectable()
export class MotorServices{
    private motorRef = this.db.list<any>('motor');

    constructor(private db: AngularFireDatabase){}

    getMotor(){
        return this.motorRef;
    }

    turnMotor(motor: motorModel, conMotor: boolean){
        return this.motorRef.update(motor.key, {
            name: "feeding the fish",
            condition: conMotor
        });
    }

    setTimer(motor: motorModel, hourFeed: number, minFeed:number, timerFeed:boolean, feedTime: Date){
        return this.motorRef.update(motor.key, {
            hour: hourFeed,
            min: minFeed,
            timerCondition: timerFeed,
            feedTime: feedTime
        });
    }

    offTimer(motor: motorModel, condition: boolean){
        return this.motorRef.update(motor.key, {
            timerCondition: condition
        });
    }
}